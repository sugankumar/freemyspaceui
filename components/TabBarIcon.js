import React from 'react';
import { Icon } from 'react-native-elements'
import { TouchableOpacity} from 'react-native'
import Colors from '../constants/Colors';

export default class TabBarIcon extends React.Component {
  render() {
    return (
      <Icon
        component={TouchableOpacity}
        name="sc-telegram"
        type="evilicon"
        size={26}
        style={{ marginBottom: -3 }}
        color={this.props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
      />
    );
  }
}