import React from 'react';
import { ScrollView, StyleSheet , Text} from 'react-native';
import { FlatList, ActivityIndicator, View  } from 'react-native';

export default class ItemsScreen extends React.Component {
  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }

  static navigationOptions = {
    title: 'Items',
  };


  componentDidMount(){
    //return fetch('http://10.0.0.17:3000/items')
    return fetch('http://10.61.67.121:3000/items')
    
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson,
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }


  render(){

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      <View style={{flex: 1, paddingTop:20}}>
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) => <Text>{item.name}, {item.description}</Text>}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }



}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
